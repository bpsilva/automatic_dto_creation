package com.bpsilva.automaticdto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomaticDtoCreationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomaticDtoCreationApplication.class, args);
	}
}
